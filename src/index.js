import express from "express";
import 'dotenv/config';


const app = express();

const PORT = process.env.PORT || 6688;

app.get ("/", (req, res)=>{
    res.send (`this is coming from ${req.path}`);
})


app.get ("/first", (req, res)=>{
    res.send (`this is coming from ${req.path}`);
})

app.listen(PORT, ()=>{
    console.log(`app is now running on ${PORT}`);
})
